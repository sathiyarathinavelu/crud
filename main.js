
//method to save data into localstorage
function save() {
    let contactList = JSON.parse(localStorage.getItem('listItem')) ?? []
    var id;
    contactList.length != 0 ? contactList.findLast((item) => id = item.id) : id = 0;
    var item = {
        checkbox : document.getElementsByName('checkbox').value,
        id: id + 1,
        names: document.getElementById('name').value,
        age: document.getElementById('age').value,
        email: document.getElementById('email').value,
        dob: document.getElementById('dobval').value,
        gender: document.querySelector('input[name="grpGender"]:checked').value
    }
    contactList.push(item)
    localStorage.setItem('listItem', JSON.stringify(contactList))
    location.reload();//reload the web page
}
//method to get all data
function allData() {
    contactList = JSON.parse(localStorage.getItem('listItem')) ?? [];
    contactList.forEach(function (value, i) {
        var table = document.getElementById('table')
        table.innerHTML +=`
                <tr>
                <td><input type='checkbox' name='checkbox' value=${i}></td>
                    <td>${i + 1}</td>
                    <td>${value.names}</td>
                    <td>${value.age}</td>
                    <td>${value.email}</td>
                    <td>${value.dob}</td>
                    <td>${value.gender}</td>
                </tr>`
    })
}
// remove all data and particular data from local storage
function getCrudData() {
    let arr = JSON.parse(localStorage.getItem('listItem'));
    return arr;
}

function setCrudData(arr) {
    localStorage.setItem('listItem', JSON.stringify(arr));
}
function deleteData() {
    localStorage.clear();
    location.reload();
}

function deleteParticularData() {
    let arr = getCrudData();
    let del=document.querySelector('input[type="checkbox"]:checked').value;
    arr.splice(del, 1); 
    setCrudData(arr);
    location.reload();
}
//update the table with checkbox
function selectiveUpload(){
    contactList = JSON.parse(localStorage.getItem('listItem')) ?? []
    var id
    contactList.length != 0 ? contactList.findLast((item) => id = item.id) : id = 0
    if(document.querySelector('input[type="checkbox"]:checked').value){
        contactList.forEach(value => {
            if(document.getElementById('id').value == value.id){
                value.name      = document.getElementById('name').value, 
                value.age       =  document.getElementById('age').value,
                value.email   =document.getElementById('email').value,
                value.dob   = document.getElementById('dobval').value,
                value.gender   = document.querySelector('input[name="grpGender"]:checked').value
            }
        });
    localStorage.setItem('listItem', JSON.stringify(contactList))
    allData()
    document.getElementById('form').reset()
}
}
